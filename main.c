// gcc employee.c -o emp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h> 

#define MAX_DATA 1000
#define EMP_NAME_LEN    30
#define EMP_ID_LEN      10
char male[MAX_DATA];
char female[MAX_DATA];
char purchaseDept[MAX_DATA];
char salesDept[MAX_DATA];
char marketingDept[MAX_DATA];


struct Employee_t {
    char name[EMP_NAME_LEN];
    char emp_id[EMP_ID_LEN];

};
typedef struct Employee_t Employee;
Employee *employee[MAX_DATA];
int id = 0;
int saved_rec_upto = 0;

void updateBitStringValue(int id, char* bitString, char value);
void saveEmployeeData(const char* name, const char* emp_id, char gender, char dept, int id);
void writeInFile(const char* filename, char* ptr);
void saveEmployeeDataInFile();
void cleanup();
void exitFromApplication();
void recoverEmployeeDataFromFile();
void readFromFile(const char* filename, char *data);

// char* readInput(const char* prompt_text){
//     char *buffer = NULL;
//     size_t size = 0;
//     printf("%s\n",prompt_text);
//     ssize_t chars = getline(&buffer, &size, stdin);
//     // Discard newline character if it is present,
//     if (chars > 0 && buffer[chars-1] == '\n') 
//         buffer[chars-1] = '\0';
//     return buffer;
// }
void readInInput(const char* prompt_text, char* buffer, int buffer_size){
    size_t size = 0;
    // printf("size:%d\n", sizeof(buffer));
    memset(buffer, 0, buffer_size);
    printf("%s ",prompt_text);
    fgets(buffer, buffer_size, stdin);
    if(!strchr(buffer, '\n'))     //newline does not exist
    {
         while(fgetc(stdin)!='\n');//discard until newline
         size = strlen(buffer);
         buffer[size-1] = '\0';
    }   
     else {
        size = strlen(buffer);
        buffer[size-1] = '\0';
     }
    
}

void enterEmployeeData(){

    char name[EMP_NAME_LEN], emp_id[EMP_ID_LEN];
    char sex[5], dept[5];
    char ch = 'y';

    while (1)
    {
        
        readInInput("Enter Name: ", name, EMP_NAME_LEN);
        readInInput("Enter EMP_ID:",emp_id, EMP_ID_LEN);
        readInInput("Enter Gender [1 - Male, 0 - Female]:",  sex, 5);
        readInInput("Enter Department [1-PURCHASE, 2-SALES, 3-MARKETING ]",dept, 5);

        
        if(sex[0] != '1' && sex[0] != '0')
        {
            printf("Invalid gender input. Please fill all detail again\n");
            continue;
        }
        if(dept[0] != '1' && dept[0] != '2' && dept[0] != '3')
        {
            printf("Invalid department input. Please fill all detail again\n");
            continue;
        }
        
        saveEmployeeData(name, emp_id, sex[0], dept[0], id);
        id++;
        printf("Press any key to continue. Press \'q\' to quit:");
        ch = getchar();
        if(ch == 'q' || ch =='Q')
            break;
    }
    
}
void updateBitStringValue(int id, char* bitString, char value){
    // char* tmp = NULL;
    // if(bitString == NULL){
    //     bitString = (char*)malloc(100);
    //     memset(bitString, 0, sizeof(bitString));
    // }
    // printf("size of bitString: %d\n", sizeof(bitString));
    // if(sizeof(bitString) < id){
    //     tmp = (char* )realloc(bitString,id+100);
    //     if(tmp != NULL)
    //         bitString = tmp;
    // }
    bitString[id] = value;
    // printf("%s\n", bitString);
}
void saveEmployeeData(const char* name, const char* emp_id, char gender, char dept, int id){
    Employee *emp  = (Employee*) malloc(sizeof(Employee));
    strcpy(emp->name, name);
    strcpy(emp->emp_id, emp_id);

    employee[id] = emp;
    if(gender == '1'){
        updateBitStringValue(id, male,'1');    
        updateBitStringValue(id, female,'0');
    }
    else if(gender == '0'){
        updateBitStringValue(id, male,'0');    
        updateBitStringValue(id, female,'1');
    }
    updateBitStringValue(id, purchaseDept, dept=='1'?'1':'0');
    updateBitStringValue(id, salesDept, dept=='2'?'1':'0');
    updateBitStringValue(id, marketingDept, dept=='3'?'1':'0');
    
}

void writeInFile(const char* filename, char* ptr)
{
    FILE *fptr;
    if ((fptr = fopen("c:\\Assignment\\.txt","w")) == NULL){
        printf("Error! opening file");
        exit(1);
    }
    if(ptr != NULL)
        fwrite(ptr, sizeof(ptr), 1, fptr); 
    fclose(fptr);
}
void saveEmployeeDataInFile()
{
    FILE *fptr;
    if ((fptr = fopen("c:\\Assignment\\.txt","a")) == NULL){
        printf("Error! opening file");
        exit(1);
    }
    for(int i = saved_rec_upto ; i< id; i++){
        if(employee[i] != NULL )
            fwrite(employee[i], sizeof(Employee), 1, fptr); 
    }
    saved_rec_upto = id;
    fclose(fptr);
    writeInFile("emp_male_data.dat", male);
    writeInFile("emp_female_data.dat", female);
    writeInFile("emp_purchase_data.dat", purchaseDept);
    writeInFile("emp_marketing_data.dat", marketingDept);
    writeInFile("emp_sales_data.dat", salesDept);

} 
void displayEmployeeData(int input){
    int i, index = 1;
    char dept, sex;
    Employee *tmp;
    if(input == 3)
        printf("\n\r%-7s%-30s%-10s%-5s%-20s\n","Index", "Name","EMP_ID","Gender","Department");
    else if(input == 5){
        printf("Enter Department [1-PURCHASE, 2-SALES, 3-MARKETING] for listing the records:");
        scanf("%c",&dept);
        if(dept == '1' || dept == '2' || dept == '3'){
            printf("\n\r%-7s%-30s%-30s%s)\n","Index","Name","EMP_ID  (Of Department:", 
            dept =='1'?"Purchase":(dept == '2'?"Sales":"Marketing"));
        } else {
            printf("Error: Wrong Input!\n");
            return;
        }
    }
    else if(input == 6){
        printf("Enter Gender [1 - Male, 0 - Female]: for listing the records:");
        scanf("%c",&sex);
        if(sex == '0' || sex == '1'){
            printf("\n\r%-7s%-30s%-30s%s)\n","Index","Name","EMP_ID  (Of Department:", 
            sex =='1'?"Male":"Female");
        } else {
            printf("Error: Wrong Input!\n");
            return;
        }
    }
    else if(input == 7){
        printf("Enter Department [1-PURCHASE, 2-SALES, 3-MARKETING] for listing the records:");
        scanf("%c%*c", &dept);
        if(!(dept == '1' || dept == '2' || dept == '3')) {
            printf("Error: Wrong Input!\n");
            return;
        }

        printf("Enter Gender [1 - Male, 0 - Female]: for listing the records:");
        scanf("%c%*c", &sex);
        if(!(sex == '0' || sex == '1')){
            printf("Error: Wrong Input!\n");
            return;
        }
        printf("\n\r%-7s%-30sEMP_ID  (Of Department: %s and Gender: %s)\n","Index","Name",
        dept =='1'?"Purchase":(dept == '2'?"Sales":"Marketing"),sex =='1'?"Male":"Female");
    }
    for(i = 0; i < id; i++)
    {
        tmp = employee[i];
        if(input == 3)  // Display All records(type table 1.)
            printf("%-7d%-30s%-10s%-5s%-20s\n", index++,tmp->name, tmp->emp_id, male[i]=='1'?"Male":"Female", 
                    purchaseDept[i] =='1'?"Purchase":(marketingDept[i] == '1'?"Marketing":"Sales"));
        else if (input == 5)
        { //Based on department
            if(dept == '1' && purchaseDept[i] =='1')
                printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            else if(dept == '2' && salesDept[i] =='1')
                printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            else if(dept == '3' && marketingDept[i] =='1')
                printf("%-5d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
         }
        else if (input == 6)
        {  // Based upon Gender
            if(sex == '1' && male[i] =='1')
                printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            else if(sex == '0' && female[i] =='1')
                printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
         }
        else if (input == 7)
        { //Based on department & Gender
            if(dept == '1' && purchaseDept[i] =='1')
            {
                if(sex == '1' && male[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
                else if(sex == '0' && female[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            }
            else if(dept == '2' && salesDept[i] =='1')
            {
                if(sex == '1' && male[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
                else if(sex == '0' && female[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            }
            else if(dept == '3' && marketingDept[i] =='1')
            {
                if(sex == '1' && male[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
                else if(sex == '0' && female[i] =='1')
                    printf("%-7d%-30s%-10s\n", index++,tmp->name, tmp->emp_id);
            }
         }
    }
}
void readFromFile(const char* filename, char *data){
    FILE *fptr;
    if ((fptr = fopen(filename,"r")) == NULL){
        printf("Error! opening file");
        return;
    }
    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);  /* same as rewind(f); */
    if(fsize == 0)
        return;
    fread(data, 1, fsize, fptr);
    fclose(fptr);
}

void recoverEmployeeDataFromFile(){
    FILE *fptr;
    if(saved_rec_upto < id){
        saveEmployeeDataInFile();
    }
    Employee tmp;
    if ((fptr = fopen("emp_data.dat","r")) == NULL){
        printf("Error! opening file");
        exit(1);
    }
    id = 0;
    // read file contents till end of file 
    while(fread(&tmp, sizeof(Employee), 1, fptr)) {
        // printf("%s %s\n", tmp.name, tmp.emp_id);
        Employee *emp  = (Employee*) malloc(sizeof(Employee));
        strcpy(emp->name, tmp.name);
        strcpy(emp->emp_id, tmp.emp_id);
        employee[id] = emp;
        id++;
    }
    saved_rec_upto = id;
    fclose(fptr);
    readFromFile("emp_male_data.dat", male);
    readFromFile("emp_female_data.dat", female);
    readFromFile("emp_purchase_data.dat", purchaseDept);
    readFromFile("emp_marketing_data.dat", marketingDept);
    readFromFile("emp_sales_data.dat", salesDept);
}

void cleanup(){
    int i = 0;
    Employee *tmp;
    for(i = 0; i < id; i++){
        tmp = employee[i];
        if(tmp != NULL){
            free(tmp);
            tmp = NULL;
        }
        employee[i] = NULL;
    }
}
void exitFromApplication(){
    char ch;
    printf("Do you want to save records in file and exit? Press\'Y\'._");
    scanf("%c",&ch);
    if(ch == 'y'|| ch =='Y')
        saveEmployeeDataInFile();
    printf("Bye Bye....\n");
    cleanup();
    exit(0);
}


int main(int argc, char const *argv[])
{
    int input;
    while(1){
        printf("Please enter your choices:\n");
        printf("\r1. Press 1 for enter the record\n");
        printf("\r2. Press 2 save in txt file\n");
        printf("\r3. Press 3 display employee data\n");
        printf("\r4. Press 4 recover employee data from file\n");
        printf("\r5. Press 5 display employee name & EMP_ID working in selected department\n");
        printf("\r6. Press 6 display employee name & EMP_ID with selected sex type\n");
        printf("\r7. Press 7 display employee name & EMP_ID with selected sex type & working in selected department \n");
        printf("\r8. Press 8 for exit\n");

        scanf("%d%*c", &input);
        // scanf("%d", &input);
        switch(input){
            case 1:
                    enterEmployeeData();
                    break;
            case 2:
                    saveEmployeeDataInFile(); 
                    break;
            case 3:
                    displayEmployeeData(input); 
                    break;
            case 4:
                    recoverEmployeeDataFromFile(); 
                    break;
            case 5:
            case 6:
            case 7:
                    displayEmployeeData(input); 
                    break;
            case 8:
                    exitFromApplication(); 
                    break;
            
        }
   }
    
}
